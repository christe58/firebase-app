﻿using GalaxyAPI.Gateway.Aliyun.Util;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace AliyunApiGateway
{
    public class AliyunMessageHandler : DelegatingHandler
    {
        private readonly string _appKey;
        private readonly string _appSecret;
        private readonly string _caStage;
        private readonly HashAlgorithm _md5provider;

        public AliyunMessageHandler(string appKey, string appSecret, string stage = Constants.CA_STAGE_RELEASE)
        {
            _appKey = appKey;
            _appSecret = appSecret;
            _caStage = stage;
            InnerHandler = new HttpClientHandler();
            _md5provider = new MD5CryptoServiceProvider();
        }
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (!request.Headers.Date.HasValue)
            {
                request.Headers.Date = DateTimeOffset.Now;
            }
            request.Headers.Add(SystemHeader.X_CA_TIMESTAMP, DateUtil.ConvertDateTimeInt(DateTime.Now));
            request.Headers.Add(SystemHeader.X_CA_NONCE, Guid.NewGuid().ToString());
            request.Headers.Add(SystemHeader.X_CA_KEY, _appKey);
            request.Headers.Add(SystemHeader.X_CA_STAGE, _caStage);
            request.Headers.Add(SystemHeader.X_CA_SIGNATURE_HEADERS, string.Join(",", SignHeader()));

            var query = ParseQueryString(request.RequestUri.Query);
            Dictionary<string, string> body = null;
            if (request.Content?.Headers.ContentType.MediaType == MediaTypeWithQualityHeaderValue.Parse(ContentType.CONTENT_TYPE_FORM).MediaType)
            {
                body = ParseQueryString(await request.Content.ReadAsStringAsync());
            }
            else
            {
                if (request.Method == System.Net.Http.HttpMethod.Put || request.Method == System.Net.Http.HttpMethod.Post)
                {
                    var source = await request.Content.ReadAsStreamAsync();
                    request.Content.Headers.ContentMD5 = _md5provider.ComputeHash(source);
                }
                body = new Dictionary<string, string>();
            }
            var signStr = SignUtil.Sign(request.RequestUri.LocalPath, request.Method.ToString(), _appSecret, AsHeaderDictionary(request.Headers, request.Content?.Headers), query, body, SignHeader());
            request.Headers.Add(SystemHeader.X_CA_SIGNATURE, signStr);
            return await base.SendAsync(request, cancellationToken);
        }

        private static Dictionary<string, string> AsHeaderDictionary(HttpRequestHeaders requestHeaders, HttpContentHeaders contentHeaders)
        {
            var headers = new Dictionary<string, string>
            {
                { HttpHeader.HTTP_HEADER_ACCEPT, requestHeaders.Accept?.ToString() },
                { HttpHeader.HTTP_HEADER_DATE, requestHeaders.Date?.UtcDateTime.ToString(CultureInfo.GetCultureInfo("en-US").DateTimeFormat.RFC1123Pattern) },
                { HttpHeader.HTTP_HEADER_CONTENT_MD5, contentHeaders == null ? null : Convert.ToBase64String(contentHeaders?.ContentMD5) },
                { HttpHeader.HTTP_HEADER_CONTENT_TYPE, contentHeaders?.ContentType?.ToString() }
            };
            var custom = requestHeaders.ToDictionary(x => x.Key, x => x.Value.First(y => !string.IsNullOrEmpty(y)));
            custom.Remove(SystemHeader.X_CA_SIGNATURE);
            custom.Remove(SystemHeader.X_CA_SIGNATURE_HEADERS);
            custom.Remove(HttpHeader.HTTP_HEADER_ACCEPT);
            custom.Remove(HttpHeader.HTTP_HEADER_DATE);
            custom.Remove(HttpHeader.HTTP_HEADER_CONTENT_MD5);
            custom.Remove(HttpHeader.HTTP_HEADER_CONTENT_TYPE);

            foreach (var item in custom)
            {
                headers.Add(item.Key, item.Value);
            }
            return headers;
        }

        private static List<string> SignHeader()
        {
            return new List<string>()
            {
                SystemHeader.X_CA_TIMESTAMP, SystemHeader.X_CA_KEY, SystemHeader.X_CA_NONCE, SystemHeader.X_CA_STAGE
            };
        }

        private static Dictionary<string, string> EmptyDirectary()
        {
            return new Dictionary<string, string>();
        }
        public static Dictionary<string, string> ParseQueryString(string s)
        {
            Dictionary<string, string> nvc = new Dictionary<string, string>();

            // remove anything other than query string from url
            if (s.Contains("?"))
            {
                s = s.Substring(s.IndexOf('?') + 1);
            }

            foreach (string vp in Regex.Split(s, "&"))
            {
                if (vp.Length == 0) continue;

                string[] singlePair = Regex.Split(vp, "=");
                if (singlePair.Length == 2)
                {
                    nvc.Add(singlePair[0], singlePair[1]);
                }
                else
                {
                    // only one key with no value specified in query string
                    nvc.Add(singlePair[0], string.Empty);
                }
            }

            return nvc;
        }
    }
}
