﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliyunApiGateway
{
    public class Constants
    {
        //签名算法HmacSha256
        public const String HMAC_SHA256 = "HmacSHA256";
        //编码UTF-8
        public const String ENCODING = "UTF-8";
        //UserAgent
        public const String USER_AGENT = "demo/aliyun/net";
        //换行符
        public const String LF = "\n";
        //分隔符1
        public const String SPE1 = ",";
        //分隔符2
        public const String SPE2 = ":";

        //默认请求超时时间,单位毫秒
        public const int DEFAULT_TIMEOUT = 1000;

        //参与签名的系统Header前缀,只有指定前缀的Header才会参与到签名中
        public const String CA_HEADER_TO_SIGN_PREFIX_SYSTEM = "X-Ca-";

        public const string CA_STAGE_TEST = "TEST";
        public const string CA_STAGE_PRE = "PRE";
        public const string CA_STAGE_RELEASE = "RELEASE";
    }

    public class SystemHeader
    {

        //签名Header
        public const String X_CA_SIGNATURE = "X-Ca-Signature";
        //所有参与签名的Header
        public const String X_CA_SIGNATURE_HEADERS = "X-Ca-Signature-Headers";
        //请求时间戳
        public const String X_CA_TIMESTAMP = "X-Ca-Timestamp";
        //请求放重放Nonce,15分钟内保持唯一,建议使用UUID
        public const String X_CA_NONCE = "X-Ca-Nonce";
        //APP KEY
        public const String X_CA_KEY = "X-Ca-Key";
        //请求API所属Stage
        public const String X_CA_STAGE = "X-Ca-Stage";

    }
    public class ContentType
    {
        //表单类型Content-Type
        public const String CONTENT_TYPE_FORM = "application/x-www-form-urlencoded; charset=utf-8";
        // 流类型Content-Type
        public const String CONTENT_TYPE_STREAM = "application/octet-stream; charset=utf-8";
        //JSON类型Content-Type
        public const String CONTENT_TYPE_JSON = "application/json; charset=utf-8";
        //XML类型Content-Type
        public const String CONTENT_TYPE_XML = "application/xml; charset=utf-8";
        //文本类型Content-Type
        public const String CONTENT_TYPE_TEXT = "application/text; charset=utf-8";

    }

    public class HttpHeader
    {
        //请求Header Accept
        public const String HTTP_HEADER_ACCEPT = "Accept";
        //请求Body内容MD5 Header
        public const String HTTP_HEADER_CONTENT_MD5 = "Content-MD5";
        //请求Header Content-Type
        public const String HTTP_HEADER_CONTENT_TYPE = "Content-Type";
        //请求Header UserAgent
        public const String HTTP_HEADER_USER_AGENT = "User-Agent";
        //请求Header Date
        public const String HTTP_HEADER_DATE = "Date";
    }
}
