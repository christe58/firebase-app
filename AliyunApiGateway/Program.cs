﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AliyunApiGateway
{
    class Program
    {
        static void Main(string[] args)
        {
            //APP ID:6300676
            using (var client = new HttpClient(new AliyunMessageHandler("24815959", "7d756aa85b2b0d0fbbd391bcd8609300", Constants.CA_STAGE_RELEASE)) {
                BaseAddress = new Uri("http://54606fcab0354cd0864dee2e74596925-cn-hongkong.alicloudapi.com")
            })
            {
                var data = new StringContent(
                    JsonConvert.SerializeObject(new { Staff = "12345678" }),
                    Encoding.UTF8, "application/json");
                var response = client.PostAsync("/api/v1/user/signup", data)
                    .GetAwaiter().GetResult();
                var body = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                Console.WriteLine(body);
                Console.Read();
            }
        }
    }
}
